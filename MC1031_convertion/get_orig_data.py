import os


def fileInDir(direc, fileName):
    return os.path.join(direc, fileName)

def read_file(file_in_path):
    f = open(file_in_path)
    data = f.readlines()
    f.close()
    return data

def get_new_name_from_old(atom_old_name, conversion_table):
    #print atom_old_name
    name = 'NaN'
    try:
        name = conversion_table[atom_old_name]
    except:
        pass
    #print name
    return name

def read_conversion_table(direc):
    fileName = 'TableBase.csv'
    file_in_path = fileInDir(direc, fileName)
    f = open(file_in_path,'r')
    lines = f.readlines()
    f.close()

    conversion_table = {}
    for l in lines:
        l = l.replace('"','').replace('\n','')
        l = l.split(';')
        #print l

        aa = l[0]
        new_C_pos = l[22]
        new_C_name = aa+' '+new_C_pos
        #print  l[1]
        
        old_C_pos = l[2]
        if len(old_C_pos)>0:
            spin_system = l[21]
            old_name = 's'+spin_system+old_C_pos
            #print 'C', old_name, new_name
            conversion_table[old_name] = 'C'+new_C_name

        new_H_pos = l[23]
        new_H_name = aa+' '+new_H_pos
        old_H_pos = l[8]
        if len(old_H_pos)>0:
            spin_system = l[21]
            old_name = 's'+spin_system+old_H_pos
            #print 'H', old_name, new_name
            conversion_table[old_name] = 'H'+new_H_name

    return conversion_table
    

def get_atom_list_data(direc):
    """
    fileName = 'SpinsFromCaraFile.txt'
    file_in_path = fileInDir(direc, fileName)
    data = read_file(file_in_path)

    fq_data = [(l.split()[0].replace('sp',''), l.split()[1]) for l in data]
    print fq_data # atom, ppm
    #"""
    conversion_table = read_conversion_table(direc)
    
    fileName = 'AtomListSystem.prot'
    file_in_path = fileInDir(direc, fileName)
    data = read_file(file_in_path)

    atom_list_data = [l.split() for l in data]
    #for e in data:
    #    print e.split()
    #print atom_list_data # atom_nr, ppm, none, atom_old_name, spin_system

    newAtomDataFormat = {}
    number_from_old_name_dict = {}
    for e in atom_list_data:
        atom_nr, ppm, none, atom_old_name, spin_system = e
        atom_type = atom_old_name[0]
        atom_old_name = 's'+spin_system+atom_old_name
        atom_name = atom_type+atom_nr
        number_from_old_name_dict[atom_old_name]=atom_nr
        if atom_type == 'H':
            newAtomDataFormat[atom_name] = [atom_type, atom_nr, ppm, 'NaN', get_new_name_from_old(atom_old_name, conversion_table), 'NaN' ]
            #H22 [u'H', '22', u'3.189352465679668', u'UNKNOWN', u'HAdda5 9-OMe', 'none']
        elif atom_type == 'C':
            newAtomDataFormat[atom_name] = [atom_type, atom_nr, ppm, 'NaN', 'NaN', get_new_name_from_old(atom_old_name, conversion_table)]
            #C39        [u'C',  '39',   u'24.2025759684044',    u'UNKNOWN', u'1',   u'CLeu1 4']
            #'C153':    ['C',   '153',  '44.807',               'NaN',      'NaN',  'Adda5 2']
        else:
            print 'error'
    return newAtomDataFormat, number_from_old_name_dict


def get_number_from_old_name_dict(old_name, number_from_old_name_dict):
    #print number_from_old_name_dict
    try:
        number = int(old_name.replace('sp',''))
    except:
        number = number_from_old_name_dict[old_name]
    #print old_name, number
    number = str(number)
    return number
    
def extract_correlations_from_lines(lines, cor_type, number_from_old_name_dict):
    #print lines
    corelations = []
    for l in lines:
        l = l.replace('\n', '')
        #if len(l)>0 and not (l[0]=='#' or l[0]=='$' or l[0]=='*' or l[0]=='@'):
        if len(l)>0 and not (l[0] in '#$*@'):
            cor1, cor2 = l.split()[0:2]
            cor1 = get_number_from_old_name_dict(cor1, number_from_old_name_dict)
            cor2 = get_number_from_old_name_dict(cor2, number_from_old_name_dict)
            cor = [cor1, cor2, cor_type]
            #print 'cor', cor
            corelations.append(cor)
    return corelations
    
    
def get_correlations(direc, number_from_old_name_dict):
    marker = '_foundPeaks_'
    correlations = []
    for item in os.listdir(direc):
        fileInPath = fileInDir(direc, item)
        if os.path.isfile(fileInPath):
            if marker in item:
                sys, cor_type = item.replace('.txt','').replace('_p02','').split(marker)
                sys = sys.replace('sys','')
                cor_type = cor_type.upper()
                f = open(fileInPath,'r')
                lines = f.readlines()
                f.close()

                correlations+=( extract_correlations_from_lines(lines, cor_type, number_from_old_name_dict) )
    #print correlations
    return correlations


def make_atomData_carbons_and_hydrogens_from_newAtomDataFormat(newAtomDataFormat):
    Carbons = []
    Hydrogens = []
    atomData = {}
    
    for e in newAtomDataFormat:
        #print e, newAtomDataFormat[e]
        if newAtomDataFormat[e][0]=='H':
            Hydrogens.append(newAtomDataFormat[e][1])
        elif newAtomDataFormat[e][0]=='C':
            Carbons.append(newAtomDataFormat[e][1])
        else:
            print 'error'
        atomData[e]=newAtomDataFormat[e][2]
    
    return Carbons, Hydrogens, atomData

def write_numbers_to_old_names(number_from_old_name_dict):
    sys_to_aa = {
's7':	'Ala1',
's5':	'Arg2',
's6':	'Asp3',
's2':	'Tyr4',
's4':	'Adda5',
's3':	'Glu6',
's9':	'Mdha7',
        }
    roman_dict = {
'A': 'NaN',
'B': 'NaN',
'E': 'NaN',
'D': 'NaN',
'G': 'NaN',
'I': 'NaN',
'H': 'NaN',
'K': 'NaN',
'M': 'NaN',
'L': 'NaN',
'N': 'NaN',
'Q': 'NaN',
'X': 'NaN',
'Z': 'NaN',
}
    for element in number_from_old_name_dict:
        #print e, number_from_old_name_dict[e], number_from_old_name_dict[e]
        e = element
        if e[0:2] in sys_to_aa:
            #print 'ok'
            e = e.replace(e[0:2], sys_to_aa[e[0:2]]+' ')
            e = (e.split()[0], e.split()[1])
            
            e = e[1][0]+e[0], e[1][1:] #.replace('2','a').replace('3','b').replace('1','')
            #print e
            #print e[0], e[1]
            #if not len(e[1])>0:
                #e = e[0], '1'
            #else:
                #print roman_dict[ e[1][0] ]
            #except:
            #    print e
            #print '{}\t{}\t{}\t{}'.format(element, ' '.join(e), number_from_old_name_dict[element], ' '.join(e)[1:].split()[0])
            #print '{}\t{}\t{}\t{}\t{}'.format(element, ' '.join(e), number_from_old_name_dict[element], ' '.join(e).split()[0], ' '.join(e)[1:].split()[0],)
    
def build_data(direc):
    data_direc = 'orig_format'
    direc = os.path.join(direc,data_direc)
    NewAtomData, number_from_old_name_dict = get_atom_list_data(direc)
    #print number_from_old_name_dict
    #for e in newAtomDataFormat:
    #    print e, newAtomDataFormat[e]

    correlations = get_correlations(direc, number_from_old_name_dict)
    
    Carbons, Hydrogens, atomData = make_atomData_carbons_and_hydrogens_from_newAtomDataFormat(NewAtomData)

    write_numbers_to_old_names(number_from_old_name_dict)


    print NewAtomData
    
    return [atomData, Hydrogens, Carbons, correlations, NewAtomData]
    

def main():
    build_data(os.getcwd())

if __name__ == '__main__':
    main()


















