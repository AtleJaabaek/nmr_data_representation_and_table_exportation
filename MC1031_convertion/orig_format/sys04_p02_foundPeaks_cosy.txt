#X Y (First values are x values. !{Find whether it is f1 or f2}!).

#Diagonal peaks:
sp145	sp145
s4HZ3	s4HZ3
s4HQ3	s4HQ3
s4HI33	s4HI33
s4HI32	s4HI32
s4HH	s4HH
s4HZ2	s4HZ2
s4HQ2	s4HQ2


#sp145
# -

#s4HZ3
s4HZ3	s4HH
s4HZ3	s4HZ2


#s4HQ3
s4HQ3	s4HI33
s4HQ3	s4HI32
s4HQ3	s4HH



#s4HI33
s4HI33	s4HQ3
s4HI33	s4HI32


#s4HI32
s4HI32	s4HQ3
s4HI32	s4HI33
#Weak. close to diagonal:
s4HI32	s4HH


#s4HH
s4HH	s4HZ3
s4HH	s4HQ3
s4HH	s4HI33	#              #Weak(s4HH,s4HI33)
#Weak. close to diagonal:
s4HH	s4HI32
s4HH	s4HQ2


#s4HZ2
s4HZ2	s4HZ3


#s4HQ2
s4HQ2	s4HH


