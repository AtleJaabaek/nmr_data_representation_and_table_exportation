# '#' is the start of a comment
# '*' means that the correlation is a "Noise LV. correlation".
# '$' means that the correlation is a "possible correlation".
#X Y (First values are x values. !{Find whether it is f1 or f2}!).

#Diagonal peaks:
s6H	s6H
s6HA	s6HA
s6HB2	s6HB2
s6HB3	s6HB3


#H	
s6H	s6HA
s6H	s6HB2
s6H	s6HB3


#HA	
s6HA	s6H
s6HA	s6HB2
s6HA	s6HB3


#HB2	
s6HB2	s6H
s6HB2	s6HA
s6HB2	s6HB3


#HB3	
s6HB3	s6H
s6HB3	s6HA
s6HB3	s6HB2







