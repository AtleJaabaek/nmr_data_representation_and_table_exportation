# '#' is the start of a comment
# '*' means that the correlation is a "Noise LV. correlation".
# '$' means that the correlation is a "possible correlation".
#X Y (First values are x values. !{Find whether it is f1 or f2}!).

@label: Not all "possible correlations" marked. (Including obvious correlations because of no names)

#Diagonal peaks:
s4H	s4H
s4HD	s4HD
s4HG	s4HG
s4HB3	s4HB3
s4HA	s4HA
s4HB2	s4HB2


#H	
s4H	s4HD
s4H	s4HG
s4H	s4HB3
s4H	s4HA
s4H	s4HB2


#HG	
s4HD	s4H
s4HD	s4HG
s4HD	s4HB3
s4HD	s4HA
s4HD	s4HB2


#HB	
s4HG	s4H
s4HG	s4HD
s4HG	s4HB3
s4HG	s4HA
s4HG	s4HB2


#HA	
s4HB3	s4H
s4HB3	s4HD
s4HB3	s4HG
s4HB3	s4HA
s4HB3	s4HB2


#HF	
s4HA	s4H
s4HA	s4HD
s4HA	s4HG
s4HA	s4HB3
s4HA	s4HB2


#HX	
s4HB2	s4H
s4HB2	s4HD
s4HB2	s4HG
s4HB2	s4HB3
s4HB2	s4HA
















