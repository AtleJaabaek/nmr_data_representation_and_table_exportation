# '#' is the start of a comment
# '*' means that the correlation is a "Noise LV. correlation".
# '$' means that the correlation is a "possible correlation".
#X Y (First values are x values. !{Find whether it is f1 or f2}!).

#Diagonal peaks:
sp119	sp119
sp118	sp118
sp117	sp117
sp116	sp116
sp115	sp115


#119
sp119	sp118
sp118	sp119

sp119	sp117
sp117	sp119

sp119	sp116
sp116	sp119

sp119	sp115
sp115	sp119


#118
sp115	sp118
sp118	sp115

sp118	sp117
sp117	sp118

sp118	sp116
sp116	sp118


#117
sp117	sp116
sp116	sp117

sp117	sp115
sp115	sp117


#116
sp116	sp115
sp115	sp116




